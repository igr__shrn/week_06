package ru.edu.model;

import java.util.Date;
import java.util.UUID;

/**
 * Класс отражающий структуру хранимых в таблице полей.
 */
public class Student {

    /**
     * Первичный ключ.
     * <p>
     * Рекомендуется генерировать его только внутри
     * StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект не будет сохранен в БД, он не должен
     * иметь значение id.
     */
    private UUID id;

    /**
     * Имя.
     */
    private String firstName;

    /**
     * Фамилия.
     */
    private String lastName;

    /**
     * Дата рождения.
     */
    private Date birthDate;

    /**
     * Окончил обучение.
     */
    private boolean isGraduated;

    /**
     * Средний бал.
     */
    private double averageScore;

    /**
     * @param studentId
     * @param firstNameStudent
     * @param lastNameStudent
     * @param birthDateStudent
     * @param isGraduatedStudent
     * @param averageScoreStudent
     */
    public Student(final UUID studentId,
                   final String firstNameStudent,
                   final String lastNameStudent,
                   final Date birthDateStudent,
                   final boolean isGraduatedStudent,
                   final double averageScoreStudent) {
        this.id = studentId;
        this.firstName = firstNameStudent;
        this.lastName = lastNameStudent;
        this.birthDate = birthDateStudent;
        this.isGraduated = isGraduatedStudent;
        this.averageScore = averageScoreStudent;
    }

    /**
     * @return id
     */
    public UUID getId() {
        return id;
    }

    /**
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @return averageScore
     */
    public double getAverageScore() {
        return  averageScore;
    }

    /**
     * @return isGraduated
     */
    public boolean isGraduated() {
        return isGraduated;
    }

    /**
     * @param studentId
     */
    public void setId(final UUID studentId) {
        this.id = studentId;
    }

    /**
     * @param firstNameStudent
     */
    public void setFirstName(final String firstNameStudent) {
        this.firstName = firstNameStudent;
    }

    /**
     * @param lastNameStudent
     */
    public void setLastName(final String lastNameStudent) {
        this.lastName = lastNameStudent;
    }

    /**
     * @param birthDateStudent
     */
    public void setBirthDate(final Date birthDateStudent) {
        this.birthDate = birthDateStudent;
    }

    /**
     * @param graduatedStudent
     */
    public void setGraduated(final boolean graduatedStudent) {
        isGraduated = graduatedStudent;
    }

    /**
     * @param averageScoreStudent
     */
    public void setAverageScore(final double averageScoreStudent) {
        this.averageScore = averageScoreStudent;
    }

    /**
     *
     */
    public static class Builder {
        /**
         * ID.
         */
        private UUID id = null;
        /**
         * Имя.
         */
        private String firstName;
        /**
         * Фамилия.
         */
        private String lastName;
        /**
         * Дата рождения.
         */
        private Date birthDate;
        /**
         * Окончил обучение.
         */
        private boolean isGraduated;
        /**
         * Средний бал.
         */
        private double averageScore = .0;

        /**
         *
         * @param firstNameStudent
         * @param lastNameStudent
         * @param birthDateStudent
         */
        public Builder(final String firstNameStudent,
                       final String lastNameStudent,
                       final Date birthDateStudent) {
            this.firstName = firstNameStudent;
            this.lastName = lastNameStudent;
            this.birthDate = birthDateStudent;
        }

        /**
         *
         * @param isGraduatedStudent
         * @return isGraduated
         */
        public Builder setIsGraduatedStudent(final boolean isGraduatedStudent) {
            this.isGraduated = isGraduatedStudent;
            return this;
        }

        /**
         *
         * @param averageScoreStudent
         * @return averageScore
         */
        public Builder setAverageScore(final double averageScoreStudent) {
            this.averageScore = averageScoreStudent;
            return this;
        }

        /**
         *
         * @param studentId
         * @return studentId
         */
        public Builder setId(final UUID studentId) {
            this.id = studentId;
            return this;
        }

        /**
         *
         * @return Student
         */
        public Student build() {
            return new Student(id,
                    firstName,
                    lastName,
                    birthDate,
                    isGraduated,
                    averageScore);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Student{"
                +
                "id="
                + id
                +
                ", firstName='"
                + firstName
                + '\''
                +
                ", lastName='"
                + lastName
                + '\''
                +
                ", birthDate="
                + birthDate
                +
                ", isGraduated="
                + isGraduated
                +
                ", averageScore="
                + averageScore
                +
                '}';
    }
}
