package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class StudentTest {
    private final String STUDENT_FIRST_NAME = "first_name";
    private final String STUDENT_LAST_NAME = "last_name";
    private Date studentBirthday;

    UUID id;
    Student student;

    @Before
    public void setup(){
        Date utilDate = new Date();
        Date sqlDate = new Date(utilDate.getTime());
        studentBirthday = sqlDate;
        id = UUID.randomUUID();
        student = new Student.Builder(STUDENT_FIRST_NAME, STUDENT_LAST_NAME, studentBirthday)
                .setId(id)
                .build();
    }

    @Test
    public void testGetFirstName() {
        assertEquals(STUDENT_FIRST_NAME, student.getFirstName());
    }

    @Test
    public void testSetFirstName() {
        student.setFirstName("Test");
        assertEquals("Test", student.getFirstName());
    }

    @Test
    public void testGetLastName() {
        assertEquals(STUDENT_LAST_NAME, student.getLastName());
    }

    @Test
    public void testSetLastName() {
        student.setLastName("Test");
        assertEquals("Test", student.getLastName());
    }

    @Test
    public void testGetBirthday() {
        assertEquals(studentBirthday, student.getBirthDate());
    }

    @Test
    public void testSetBirthday() {
        Date utilDate = new Date();
        Date sqlDate = new Date(utilDate.getTime());
        student.setBirthDate(sqlDate);
        assertEquals(sqlDate, student.getBirthDate());
    }

    @Test
    public void testGetIsGraduated() {
        assertEquals(false, student.isGraduated());
    }

    @Test
    public void testSetGraduated() {
        student.setGraduated(true);
        assertEquals(true, student.isGraduated());
    }

    @Test
    public void testGetAverage() {
        assertThat(0.0, equalTo(student.getAverageScore()));
    }

    @Test
    public void testSetAverage() {
        student.setAverageScore(10);
        assertThat(10.0, equalTo(student.getAverageScore()));
    }
}