package ru.edu;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import liquibase.Contexts;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.junit.Before;
import org.junit.Test;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import org.mockito.Mock;
import ru.edu.model.Student;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class StudentsRepositoryCRUDTest {
    public static final String LIQUIBASE_CHANGELOGS_DB_CHANGELOG_XML = "liquibase/changelogs/db.changelog.xml";
    private final String JDBC_URL = "jdbc:h2:mem:edu";
    private final String STUDENT_FIRST_NAME = "first_name";
    private final String STUDENT_LAST_NAME = "last_name";

    StudentsRepositoryCRUD crud;
    Connection connection;
    Date date;
    String dateTest;

    @Mock
    Student student;

    @Mock
    Student studentTmp;

    @Before
    public void setup() throws SQLException, LiquibaseException {
        openMocks(this);
        date = new Date();

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        dateTest = sdf.format(cal.getTime());

        UUID id = UUID.randomUUID();
        when(student.getId()).thenReturn(id);
        when(student.getFirstName()).thenReturn(STUDENT_FIRST_NAME);
        when(student.getLastName()).thenReturn(STUDENT_LAST_NAME);
        when(student.getBirthDate()).thenReturn(date);
        when(student.isGraduated()).thenReturn(false);
        when(student.getAverageScore()).thenReturn(0.0);

        connection = DriverManager.getConnection(JDBC_URL);
        crud = new StudentsRepositoryCRUDImpl(connection);

        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
        Liquibase liquibase = new Liquibase(LIQUIBASE_CHANGELOGS_DB_CHANGELOG_XML, new ClassLoaderResourceAccessor(), database);
        liquibase.update(new Contexts(""));
    }

    @Test
    public void testCreateAndSelectById() {
        try {
            UUID id = crud.create(student);
            studentTmp = crud.selectById(id);

            assertEquals(id, studentTmp.getId());
            assertEquals(STUDENT_FIRST_NAME, studentTmp.getFirstName());
            assertEquals(STUDENT_LAST_NAME, studentTmp.getLastName());
            assertEquals(dateTest, studentTmp.getBirthDate().toString());
            assertEquals(false, studentTmp.isGraduated());
            assertThat(0.0, equalTo(studentTmp.getAverageScore()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateWithNullIdStudent() {
        try {
            UUID id = crud.create(student);
            studentTmp = crud.selectById(id);
            studentTmp.setId(null);
            crud.create(studentTmp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdate(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        calendar.add(1, -10);
        String dateTestInString = formatDate.format(calendar.getTime());

        Date dateUpdate = new Date();
        try {
            dateUpdate = formatDate.parse(dateTestInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            UUID id = crud.create(student);
            studentTmp = crud.selectById(id);

            studentTmp.setFirstName("testFirstname");
            studentTmp.setLastName("testLastname");
            studentTmp.setBirthDate(dateUpdate);
            studentTmp.setGraduated(true);
            studentTmp.setAverageScore(9.9);

            int affectedRows = crud.update(studentTmp);
            assertEquals(1, affectedRows);

            Student studentTmp1 = crud.selectById(id);

            assertEquals("testFirstname", studentTmp1.getFirstName());
            assertEquals("testLastname", studentTmp1.getLastName());
            assertEquals(dateTestInString, studentTmp1.getBirthDate().toString());
            assertEquals(true, studentTmp1.isGraduated());
            assertThat(9.9, equalTo(studentTmp1.getAverageScore()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSelectByIdNoIdInTable(){
        UUID testId = UUID.randomUUID();
        try {
            assertEquals(null, crud.selectById(testId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRemove(){
        List<UUID> uuidList = new ArrayList<>();
        try {
            UUID id1 = crud.create(student);
            uuidList.add(id1);

            UUID id2 = crud.create(student);
            uuidList.add(id2);

            int countRemove = crud.remove(uuidList);
            assertEquals(2, countRemove);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSelectAll(){
        List<Student> students;
        try {
            crud.create(student);
            crud.create(student);
            students = crud.selectAll();
            assertEquals(2, students.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}