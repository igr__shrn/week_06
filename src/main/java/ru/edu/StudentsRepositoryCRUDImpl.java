package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {

    /**
     * Соединение с базой.
     */
    private final Connection connection;

    /**
     * @param connect
     */
    public StudentsRepositoryCRUDImpl(final Connection connect) {
        this.connection = connect;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        if (student.getId() != null) {
            update(student);
        }

        UUID id = UUID.randomUUID();

        String sql = "insert into students values("
                + "'" + id + "',"
                + "'" + student.getFirstName() + "',"
                + "'" + student.getLastName() + "',"
                + "'" + new java.sql.Date(
                student.getBirthDate()
                        .getTime()) + "',"
                + "" + student.isGraduated() + ","
                + "" + student.getAverageScore() + ")";

        try (Statement statement = connection.createStatement()) {
            boolean rs = statement.execute(sql);
            System.out.println("Добавление студента прошло успешно"
                    + " hasResultSet " + rs);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return id;
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return
     */
    @Override
    public Student selectById(final UUID id) {
        String sql = "select * from students where id = '" + id + "'";

        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            if (!rs.next()) {
                return null;
            }

            return getStudent(rs);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Student getStudent(final ResultSet rs) {
        try {

            return new Student.Builder(rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getDate("birth_date"))
                    .setId(UUID.fromString(rs.getString("id")))
                    .setIsGraduatedStudent(rs.getBoolean("is_graduated"))
                    .setAverageScore(rs.getDouble("average_score"))
                    .build();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Получение всех записей из БД.
     *
     * @return
     */
    @Override
    public List<Student> selectAll() {
        String sql = "select * from students";
        List<Student> students = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                Student studentTmp = getStudent(rs);
                students.add(studentTmp);
            }

            return students;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {
        String sql = "update students set "
                + "first_name='" + student.getFirstName() + "',"
                + "last_name='" + student.getLastName() + "',"
                + "birth_date='" + new java.sql.Date(
                student.getBirthDate()
                        .getTime()) + "',"
                + "is_graduated=" + student.isGraduated() + ","
                + "average_score=" + student.getAverageScore() + " "
                + "where id='" + student.getId() + "'";

        try (Statement statement = connection.createStatement()) {
            int affectedRows = statement.executeUpdate(sql);

            return affectedRows;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Удаление указанных записей по id.
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {
        StringBuilder sqlQuery = new StringBuilder();
        String str = idList.stream()
                .map(s -> "'" + s.toString() + "'")
                .collect(Collectors.joining(","));
        sqlQuery.append("delete from students where id in(")
                .append(str).append(')');

        try (Statement stmt = connection.createStatement()) {
            int countDeleted = stmt.executeUpdate(sqlQuery.toString());

            return countDeleted;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
