package ru.edu;

import ru.edu.model.Student;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Интерфейс, описывающий логику работы
 * с таблицеq Students.
 */
public interface StudentsRepositoryCRUD {

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    UUID create(Student student) throws SQLException;

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return Student
     */
    Student selectById(UUID id) throws SQLException;

    /**
     * Получение всех записей из БД.
     *
     * @return List<Student>
     */
    List<Student> selectAll();

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    int update(Student student);

    /**
     * Удаление указанных записей по id.
     *
     * @param idList
     * @return количество удаленных записей
     */
    int remove(List<UUID> idList);
}
